<?php

Class Setup_Metal_API {
	public $db;
	public $user = 'root';
	public $pass = '';
	public $host = 'localhost';
	public $dbname = 'metal_api';
	public $table = 'metals';

	public function __construct() {
		try {
			$this->db = new PDO("mysql:dbname=$this->dbname;host=$this->host", $this->user, $this->pass);
			var_dump($this->db);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "CREATE table $this->table(
				ID INT(11) AUTO_INCREMENT PRIMARY KEY,
				gold_bid_price float(12,2) NOT NULL,
				gold_bid_price_date TIMESTAMP NOT NULL,
				gold_ask_price float(12,2) NOT NULL,
				gold_ask_price_date TIMESTAMP NOT NULL,
				gold_price_change float(12,2) NOT NULL,
				gold_price_change_date TIMESTAMP NOT NULL,
				gold_percentage_change float(12,2) NOT NULL,
				gold_percentage_change_date TIMESTAMP NOT NULL,
				silver_bid_price float(12,2) NOT NULL,
				silver_bid_price_date TIMESTAMP NOT NULL,
				silver_ask_price float(12,2) NOT NULL,
				silver_ask_price_date TIMESTAMP NOT NULL,
				silver_price_change float(12,2) NOT NULL,
				silver_price_change_date TIMESTAMP NOT NULL,
				silver_percentage_change float(12,2) NOT NULL,
				silver_percentage_change_date TIMESTAMP NOT NULL,
				platinum_bid_price float(12,2) NOT NULL,
				platinum_bid_price_date TIMESTAMP NOT NULL,
				platinum_ask_price float(12,2) NOT NULL,
				platinum_ask_price_date TIMESTAMP NOT NULL,
				platinum_price_change float(12,2) NOT NULL,
				platinum_price_change_date TIMESTAMP NOT NULL,
				platinum_percentage_change float(12,2) NOT NULL,
				platinum_percentage_change_date TIMESTAMP NOT NULL,
				palladium_bid_price float(12,2) NOT NULL,
				palladium_bid_price_date TIMESTAMP NOT NULL,
				palladium_ask_price float(12,2) NOT NULL,
				palladium_ask_price_date TIMESTAMP NOT NULL,
				palladium_price_change float(12,2) NOT NULL,
				palladium_price_change_date TIMESTAMP NOT NULL,
				palladium_percentage_change float(12,2) NOT NULL,
				palladium_percentage_change_date TIMESTAMP NOT NULL,
				all_price text NOT NULL,
				all_price_date TIMESTAMP NOT NULL);";
			$this->db->exec($sql);
			print("Created $this->table on $this->dbname\r\n");
		} catch(PDOException $e) {
			echo $e->getMessage();
		}

	}


}