<?php
Class Database {

	private $host = 'localhost';
	private $user = 'root';
	private $pass = '';
	private $dbname = 'metal_api';
	private $table = 'metals';

	private $dbh;
	private $error;

	private $statement;

	public function __construct() {
		$dsn = "mysql:host=$this->host;dbname=$this->dbname;";
		$options = array(
			PDO::ATTR_PERSISTENT	=> true,
			PDO::ATTR_ERRMODE		=> PDO::ERRMODE_EXCEPTION
		);

		try {
			$this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
		}

		catch(PDOException $e) {
			$this->error = $e->getMessage();
		}
	}

	public function query($query) {
		$this->statement = $this->dbh->prepare($query);
	}

	public function bind($param, $value, $type = null) {
		if(is_null($type)) {
			switch(true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->statement->bindValue($param, $value, $type);
	}

	public function execute() {
		return $this->statement->execute();
	}

	public function result_set() {
		$this->execute();
		return $this->statement->fetchAll(PDO::FETCH_ASSOC);
	}

	public function single() {
		$this->execute();
		return $this->statement->fetch(PDO::FETCH_ASSOC);
	}

	/* Not needed but included because learning */
	public function row_count() {
		return $this->statement->rowCount();
	}
	public function last_insert_id() {
		return $this->dhb->lastInserId();
	}

	public function begin_transaction() {
		return $this->dbh->beginTransaction();
	}
	public function end_transaction() {
		return $this->dbh->commit();
	}
	public function cancel_transaction() {
		return $this->dbh->rollBack();
	}

	public function debug_dump_params() {
		return $this->statement->debugDumpParams();
	}
	
}