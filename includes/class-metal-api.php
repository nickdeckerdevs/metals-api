<?php


class Metal_API {
	public $query;
	public $updated_last;
	public $feed;
	public $db;
	public $update;
	public $price;
	public $now;
	public $intervals = array('s' => 1, 'i' => 60, 'h' => 3600, 'd' => 86400, 'm' => 2592000, 'y' => 31536000);


	/* Check to see if we have recent price update */
	public function __construct($query) {
		$this->query = $query;
		$this->db = new Database();
		$this->update = $this->get_last_updated_time();
		
	}

	public function get_last_updated_time() {
		$this->db->query("SELECT " . $this->query . "_date, " . $this->query . " FROM metals WHERE ID = 1");
		$db_time = $this->db->single();
		$this->updated_last = $db_time[$this->query . '_date'];
		$this->price = $db_time[$this->query];
		$this->now = new DateTime;
		if( $this->calculate_difference( $this->now, new DateTime($this->updated_last)) == 'update' ) {
			return 1;
		} 
		return 0;
	} 

	public function calculate_difference($now, $previous) {
		$difference = $now->diff($previous);
		$total_time = 0;
		foreach($this->intervals as $interval => $value) {
			$total_time += $difference->$interval * $value;
			if($total_time >= 60) {
				return 'update';
			}
		}
		return;
	}
	
	public function update_db_values($json = false) {
		if($json == true) {
			$fix = $this->fix_json();
		}
		$this->db->query("UPDATE metals SET $this->query = :price, " . $this->query . "_date = :date WHERE ID = 1");
		$this->db->bind(":price", $this->feed);
		$this->db->bind(":date", $this->now->format('Y-m-d H:i:s'));
		$execution = $this->db->execute();
	}
	public function fix_json() {
		$this->feed = substr($this->feed, 0, -2) . ', "updated" : "' . $this->now->format('Y-m-d H:i:s') . '" }';
		echo $this->feed;
	}

	public function get_all_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('https://gold-feed.com/paid/d7d6s6d66k4j4658e6d6cds638940e/all_metals_json_usd.php');
			$this->update_db_values(true);
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s')); 
		} 
	}

	public function get_gold_bid_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801gold.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
		
	}

	public function get_gold_ask_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldask.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_gold_price_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldchangedollar.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_gold_percentage_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801goldchangepercent.php');		$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	
	public function get_silver_bid_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silver.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_silver_ask_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverask.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_silver_price_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverchangedollar.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_silver_percentage_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801silverchangepercent.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}

	public function get_platinum_bid_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinum.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_platinum_ask_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumask.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_platinum_price_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumchangedollar.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_platinum_percentage_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801platinumchangepercent.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}

	public function get_palladium_bid_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801palladium.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_palladium_ask_price() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801palladiumask.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_palladium_price_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801palladiumchangedollar.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}
	public function get_palladium_percentage_change() {
		if($this->update != 1) {
			return array('price' => $this->price, 'updated' => $this->updated_last);
		} else {
			$this->feed = file_get_contents('http://gold-feed.com/iframe/paid/55669856231478956501/55548795685212587801palladiumchangepercent.php');
			$this->update_db_values();
			return array('price' => $this->feed, 'updated' => $this->now->format('Y-m-d H:i:s'));
		}
	}

}